<?php

namespace App\Http\Controllers\Backend;

use App\Models\Intro;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class IntroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $intro = Intro::all()->first();
        return view('backend.intro.index', compact('intro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.intro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'background_image' => 'required|image',
            'profession' => 'required',
            'position' => 'required',
        ]);

        $image = $request->file('background_image');
        $slug = str_slug(config('app.name'));

        if (isset($image)) {
//            make unique name image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('uploads/intro')) {
                Storage::disk('public')->makeDirectory('uploads/intro');
            }
            $resizeImage = Image::make($image)->resize(1920, 695)->save();
            Storage::disk('public')->put('uploads/intro/' . $imageName, $resizeImage);
        } else {
            $imageName = 'default.png';
        }
        $intro = new Intro();

        $intro->first_name = $request->first_name;
        $intro->last_name = $request->last_name;
        $intro->background_image = $imageName;
        $intro->profession = $request->profession;
        $intro->position = $request->position;

        $intro->save();

        Toastr::success('Intro saved successfully!', 'Done');

        return redirect()->route('intro.index')->with('successMsg', 'Intro successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $intro = Intro::findOrFail($id);
        return view('backend.intro.edit',compact('intro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'background_image' => 'required|image',
            'profession' => 'required',
            'position' => 'required',
        ]);

        $image = $request->file('background_image');
        $slug = str_slug(config('app.name'));

        if (isset($image)) {
//            make unique name image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('uploads/intro')) {
                Storage::disk('public')->makeDirectory('uploads/intro');
            }
            $resizeImage = Image::make($image)->resize(1920, 695)->save();
            Storage::disk('public')->put('uploads/intro/' . $imageName, $resizeImage);
        } else {
            $imageName = 'default.png';
        }

        $intro = Intro::findOrFail($id);

        $intro->first_name = $request->first_name;
        $intro->last_name = $request->last_name;
        $intro->background_image = $imageName;
        $intro->profession = $request->profession;
        $intro->position = $request->position;

        $intro->update();

        Toastr::success('Intro updated successfully!', 'Done');

        return redirect()->route('intro.index')->with('successMsg', 'Intro successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }
}
