<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['category_id', 'name', 'image', 'description', 'url'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
