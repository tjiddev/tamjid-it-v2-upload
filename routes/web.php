<?php

// frontend
Route::get('/', 'Frontend\HomepageController@index')->name('homepage');
// contact
Route::post('contact','Frontend\ContactController@sendMessage')->name('contact.send');

// auth
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Backend'], function () {
    // dashboard
    Route::get('dashboard','DashboardController@index')->name('admin.dashboard');
    // category
    Route::resource('category','CategoryController');
    // project
    Route::resource('project', 'ProjectController');
    // social links
    Route::resource('social-link', 'SocialLinkController');
    // intro
    Route::resource('intro', 'IntroController');
    // user info
    Route::resource('user-info', 'UserInfoController');
    // contact info
    Route::get('contact','ContactController@index')->name('contact.index');
    Route::get('contact/{id}','ContactController@show')->name('contact.show');
    Route::delete('contact/{id}','ContactController@destroy')->name('contact.destroy');
    // Sidebar user
    View::composer('backend.partials._sidebar',function ($view){
        $info = App\Models\UserInfo::all()->last();
        $view->with('info', $info);
    });
});
