<div id="footer">
    <div class="container">
        <p>&copy; 2018 {{config('app.name')}}. All rights reserved.</p>
    </div>
</div>
{{--<script type="text/javascript" src="assets/frontend/js/jquery.1.11.1.js"></script>--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.js"></script>
{{--<script type="text/javascript" src="assets/frontend/js/bootstrap.js"></script>--}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/SmoothScroll.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/nivo-lightbox.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/jquery.isotope.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/jqBootstrapValidation.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/main.js')}}"></script>
{{--Toastr js--}}
<script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}