@extends('backend.layouts.main')

@section('title','Edit Website Intro')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}"
          rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            EDIT WEBSITE INTRO
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{route('intro.update',$intro->id)}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="first_name" class="form-control" name="first_name"
                                           value="{{$intro->first_name}}">
                                    <label class="form-label">First Name</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="last_name" class="form-control" name="last_name"
                                           value="{{$intro->last_name}}">
                                    <label class="form-label">Last Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="background_image">Background Image</label>
                                <input type="file" name="background_image" id="background_image">
                                <h3>Old Image</h3>
                                <img src="{{url('uploads/intro/',$intro->background_image)}}"
                                     alt="{{config('app.name')}}" class="img-responsive">
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="profession" class="form-control" name="profession"
                                           value="{{$intro->profession}}">
                                    <label class="form-label">Profession</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="position" class="form-control" name="position"
                                           value="{{$intro->position}}">
                                    <label class="form-label">Position</label>
                                </div>
                            </div>

                            <br>
                            <a href="{{route('intro.index')}}" type="button"
                               class="btn btn-warning m-t-15 waves-effect">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Vertical Layout | With Floating Label -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <!-- Custom Js -->
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
@endpush