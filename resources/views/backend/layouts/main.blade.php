<!DOCTYPE html>
<html>
<head>
    @include('backend.partials._header')

    @stack('css')
</head>

<body class="theme-red">

    <!-- Top Bar -->
    @include('backend.partials._navbar')
    <!-- #Top Bar -->

    <section>
        <!-- Sidebar -->
        @include('backend.partials._sidebar')
        <!-- #END# Sidebar -->
    </section>
    <section class="content">
        @yield('content')
    </section>

    @include('backend.partials._footer')

    @stack('js')

</body>
</html>