<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Welcome To | {{config('app.name')}}</title>
<!-- Favicons
        ================================================== -->
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/frontend/img/favicon/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/frontend/img/favicon/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/frontend/img/favicon/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/frontend/img/favicon/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/frontend/img/favicon/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/frontend/img/favicon/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/frontend/img/favicon/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/frontend/img/favicon/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/frontend/img/favicon/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"
      href="{{asset('assets/frontend/img/favicon/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/frontend/img/favicon/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/frontend/img/favicon/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/frontend/img/favicon/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('assets/frontend/img/favicon/manifest.json')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
      type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Waves Effect Css -->
<link href="{{asset('assets/backend/plugins/node-waves/waves.css')}}" rel="stylesheet"/>

<!-- Animation Css -->
<link href="{{asset('assets/backend/plugins/animate-css/animate.css')}}" rel="stylesheet"/>

<!-- Morris Chart Css-->
<link href="{{asset('assets/backend/plugins/morrisjs/morris.css')}}" rel="stylesheet"/>

<!-- Custom Css -->
<link href="{{asset('assets/backend/css/style.css')}}" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="{{asset('assets/backend/css/themes/all-themes.css')}}" rel="stylesheet"/>

{{--Toastr css--}}
<link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
<style>
    .pull-right {
        top: 18%;
        position: absolute;
        right: 1%;
    }
</style>